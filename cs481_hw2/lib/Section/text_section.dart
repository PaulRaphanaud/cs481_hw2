import 'package:flutter/material.dart';

class TextSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Road cycling is an activity most commonly performed on a bicycle.'
        'This is my favorite sport.'
        'I can do it everywhere.'
        'But my favorite area is the mountain, '
        'because I can climb it',
        softWrap: true,
      ),
    );
  }
}
