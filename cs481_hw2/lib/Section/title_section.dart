import 'package:flutter/material.dart';

class TitleSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 8),
                child: Text(
                  'Cycling',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Text(
                'My favorite sport',
                style: TextStyle(color: Colors.grey[500]),
              )
            ],
          )),
          Icon(Icons.calendar_today),
          SizedBox(
            width: 5,
          ),
          Text('3'),
        ],
      ),
    );
  }
}
