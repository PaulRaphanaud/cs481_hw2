import 'package:flutter/material.dart';

class ButtonSection extends StatelessWidget {
  Column _buttonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: color,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
                fontSize: 12, fontWeight: FontWeight.w400, color: color),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buttonColumn(Colors.red, Icons.directions_bike, "Sport"),
          _buttonColumn(Colors.red, Icons.panorama, "Outside"),
          _buttonColumn(Colors.red, Icons.terrain, "Montain"),
        ],
      ),
    );
  }
}
